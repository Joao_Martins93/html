
var idCount;

// Elements
 
var todoInput = document.querySelector(".todo-input");
var todoButton = document.querySelector(".todo-btn");
var todoList = document.querySelector(".todo-list");
var filterDropdown = document.querySelector(".filter-todo");


// Event listeners

todoButton.addEventListener("click", addTodo);
todoList.addEventListener("click", deleteCheck);
filterDropdown.addEventListener("change", filterTodo);



// Functions

function addTodo(event){

    event.preventDefault();

    const todoValue = todoInput.value;
    let todoCompleteValue = false;
    let todoJson = {
        "name" : todoValue,
        "complete" : todoCompleteValue,
        "id" : 'todo-${idCount}',
    };

    save(todoJson);

    idCount++;

    // criar o div
    const div = document.createElement("div");

    // const divClass = document.createAttribute("class");
    // divClass.value = "todo";
    // div.setAttributeNode(divClass);

    div.classList.add("todo");
    div.id = todoJson.id;

    // criar o elemento da lista <li>

    const todoItem = document.createElement("li");
    todoItem.classList.add("todo-item");
    todoItem.innerHTML = todoInput.value;
    div.appendChild(todoItem);


    // criar o botão de check

    const completeBtn = document.createElement("button");
    completeBtn.classList.add("complete-btn");
    completeBtn.innerHTML = '<i class="fas fa-check"></i>';
    div.appendChild(completeBtn);


    // criar botão de apagar
    // criar o botão de check

    const trashBtn = document.createElement("button");
    trashBtn.classList.add("trash-btn");
    trashBtn.innerHTML = '<i class="fas fa-trash"></i>';
    div.appendChild(trashBtn);

    // adicionar à lista de todos
    todoList.appendChild(div);

    todoInput.value = "";
}


function deleteCheck(event) {

    // obter o botão da lista que foi pressionado
    var pressedButton = event.target;

    if (pressedButton.classList[0] == "trash-btn") {
        const parent = pressedButton.parentElement;
        parent.classList.add("fall");
        parent.addEventListener("transitionend", function() {
            parent.remove();
        })
    }

    if (pressedButton.classList[0] == "complete-btn") {
        const parent = pressedButton.parentElement;
        parent.classList.toggle("complete");
        updateTodoWith(parent.id);
    }
}


function filterTodo(event){
    const todos = todoList.querySelectorAll(".todo");
    //console.log(event.target.value);

    todos.forEach(function(todo){
        switch(event.target.value){
            case "all":
                todo.style.display = "flex";
                break;

            case "completed":
                if(todo.classList.contains("complete")){
                    todo.style.display = "flex";
                } else {
                    todo.style.display = "none";
                }
                break;

            case "uncompleted":
                if(!todo.classList.contains("complete")){
                    todo.style.display = "flex";
                } else {
                    todo.style.display = "none";
                }
                break;
        }
    });
}


function updateTodoWith(id){

    // 1. obter o elemento do localstorage com o id
    let todoItems = JSON.parse(localStorage.getItem("todo"));
    const item = todoItems.filter(function(todoItem) {
        return todoItem.id == id;
    })[0];

    console.log(item);

    // 2. alterar o valor do elemento encontrado
    item.complete = !item.complete;

    // 3. guardar alterações4
    localStorage.setItem("todo", JSON.stringify(todoItems));
}


function removeItemWithId(id) {

    // 1. Buscar lista de elementos
    let todoItems = JSON.parse(localStorage.getItem("todo"));

    // 2. Remover elemento com id
    todoItems = todoItems.filter(function (todoItem){
        return todoItem.id != id;
    });

    // 3. Guardar alterações
    localStorage.setItem("todo", JSON.stringify(todoItems));
}


function loadItems() {
    if (localStorage.getItem("todo") != null) {
        let todoItems = JSON.parse(localStorage.getItem("todo"));
        todoItems.map(function (todoItem) {
            buildItemWithViewHolder(todoItem);
        });
    }
}

function buildItemWithViewHolder(viewHolder) {

    const div = document.createElement("div");

}


function save(todo){
    let todoItems;
    if (localStorage.getItem("todo") == null){
        todoItems = [];
    } else {
        todoItems = JSON.parse(localStorage.getItem("todo"));
    }
    // adicionar elemento no fim da lista
    todoItems.push(todo);
    localStorage.setItem("todo", JSON.stringify(todoItems));
}


window.onload = function() {
    if (localStorage.getItem("idCount") == null){
        idCount = 1;
    } else {
        idCount = JSON.parse(localStorage.getItem("idCount"));
    }
}

window.onunload = function() {
    localStorage.setItem = JSON.stringify(idCount);
}

window.onpagehide = function() {
    localStorage.setItem
}

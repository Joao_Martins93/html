import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faPlay, faPause, faAngleLeft, faAngleRight} from "@fortawesome/free-solid-svg-icons";

const Player = () => {
    return (
        <div className="player">
            <div className="time-control">
                <p>current time</p>

                <input type ="range" max ="5" min="1" />   

                <p>max time</p>

            </div>

            <div className="play-control">
                <FontAwesomeIcon icon = {faAngleLeft} size="2x" />
                <FontAwesomeIcon icon = {faPlay} size="2x" />
                <FontAwesomeIcon icon = {faAngleRight} size="2x" />

            </div>
        </div>
    )
}

export default Player;
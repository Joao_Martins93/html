
import LibrarySong from "./LibrarySong";

const Library = ({songs, isLibraryOpen}) => {
    return(
        <div className={`library ${isLibraryOpen ? 'open': ''}`}>
            <h2>Library</h2>
            <div className="library-songs">
                {
                    songs.map((song) => {
                        return <LibrarySong song={song}/>
                    })
                }

            </div>
        </div>
    );
}

export default Library;
import {useState} from "react";

import Player from "./Components/Player";
import Song from "./Components/Song";
import Library from "./Components/Library";
import Nav from "./Components/Nav";
import data from "./data";
import "./styles/App.scss"

function App() {

  const [songs, setSongs] = useState(data());
  const [currentSong, setCurrentSong] = useState(songs[0]);

  const [isLibraryOpen, setIsLibraryOpen] = useState(false);

  const openLibraryHandler = (event) => {
    setIsLibraryOpen(!isLibraryOpen);
  }


  return (
    <div className="App">
      <Nav openLibraryHandler = {openLibraryHandler} />
      <Song currentSong={currentSong} />
      <Player />
      <Library songs={songs} isLibraryOpen={isLibraryOpen} />
    </div>
  );
}

export default App;

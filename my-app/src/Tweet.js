const Tweet = ({tweet, tweets, setTweets}) => {

   const onRemoveHandler = (event) => {
       const newTweets = tweets.filter((tweetItem) => {
           return tweetItem !== tweet;
       });

       setTweets(newTweets);
   }


    return (
        <div className = "tweet">
            <h3>{tweet}</h3>
            <button onclick= {onRemoveHandler}>Remove</button>
            <button>Like</button>
        </div>
    );
}

export default Tweet;
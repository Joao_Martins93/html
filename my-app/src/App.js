
import {useState, useRef} from 'react';
import TweetList from './TweetList';
import {uuid} from 'uuidv4';

function App() {

  const inputRef = useRef(null);

  const [abc, setAbc] = useState("abc");
  
  
  const [tweets, setTweets] = useState([]);


  const sumitClickHandler = (event) => {
    event.preventDefault();
    setAbc("Submit");
    console.log(inputRef.current.value);
    const tweetText = inputRef.current.value;
    const newTweets = [...tweets, tweetText]
    setTweets(newTweets)
  }


  return (
    <div className="App">
      <form>
        <input type = "text" placeholder ="Write your tweets here..." ref={inputRef} />
        <button onClick={sumitClickHandler} type = "submit">{abc}</button>
      </form>

      <TweetList message="Tweets" tweets={tweets} setTweets={setTweets}/>
    </div>
  );
}

export default App;
var displayElement = document.getElementById("string");
var startingText = displayElement.textContent;
var characterList = startingText.split("");

var intervalTask = setInterval(revertOne, 500);

function revertOne(){
    var firstCharacter = characterList.shift();
    characterList.push(firstCharacter);
    displayElement.textContent = characterList.join("");
}